import datetime

from app.main.config import key
from app.main.services import db, flask_bcrypt


class Serial(db.Model):
    """ Serial Model for storing serial related details """
    __tablename__ = "serial"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    serial = db.Column(db.String(100), nullable=False, unique=True)
    serial_data = db.Column(db.String(255), nullable=False)
    created_on = db.Column(db.DateTime, nullable=False)
    location_id = db.Column(db.String(100), db.ForeignKey('location.public_id'), nullable=False)
    
    def __repr__(self):
        return "<Serial '{}' from location '{}'>".format(self.serial, self.location_id)

    