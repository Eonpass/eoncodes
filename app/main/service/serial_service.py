import datetime
import uuid

from app.main.model.validation import Validation
from app.main.model.location import Location
from app.main.model.company import Company
from app.main.model.serial import Serial
from app.main.services import db
from app.main.util.tasks import make_gossip_call
from app.main.util.eonerror import EonError
from app.main.util.keymanagementutils import KeyManagementClient
from app.main.util.hashutils import HashUtils


def create_new_serial(data):
    """
    Create a serial number attached to the chosen location with the given payload

    before creating it, check if there's a valid signature (validation item) by another company

    Parameters
    ----------
    data: dict
        the payload of the request
        'serial_data': the data to embed in the serial number
        'location_id': the location to attach the serial to

    Returns
    -------
    dict
        the response object with the new serial value
        'status'
        'message'
        'serial', the freshly created serial number

    Raises
    ------
    EonError
        400, something is wrong witht the input data
        409, the given serial already exist in the same location
    """
    location = Location.query.filter_by(public_id=data['location_id']).first()
    validations = Validation.query.filter_by(location_id=location.public_id).all()
    
    # check local integrity:
    # validaiton signed data must be location name + locaiton data:
    at_least_one_validation = False
    kmc = KeyManagementClient()
    hu = HashUtils()
    for validation in validations:
        company = Company.query.filter_by(public_id=validation.signer_company_id).first()
        payload_to_sign = location.name+location.location_key
        hashed_payload = hu.digest(payload_to_sign)
        bytes_signed_key = bytes.fromhex(validation.signed_location_key)
        is_correct = kmc.verify_signed_message(bytes_signed_key, hashed_payload, company.public_key)
        if(is_correct):
            at_least_one_validation = True
        else:
            continue

    if at_least_one_validation:
        try:
            serial = hu.hmac(validation.signed_location_key, data['serial_data']).hex()
            existing_serial = Serial.query.filter_by(serial=serial, location_id=location.public_id).first()
            if existing_serial:
                raise EonError("Another serial with the same value already exists for this location", 400)
            else:
                new_serial = Serial(
                    serial=serial,
                    serial_data=data['serial_data'],
                    created_on=datetime.datetime.utcnow(),
                    location_id=location.public_id
                )
                save_changes(new_serial) 
                return generate_creation_ok_message(new_serial)
        except Exception as e:
            print(e)
            raise EonError("Something is wrong with the serial data", 400)
    else:
        raise EonError("No correct validations available for the location", 400)

def create_serials_batch(data):
    """
    Use a similar payload and only increment a unit id for all the serials to be emitted

    append to the serial_data an incremental id and try to insert the new serials

    Parameters
    ----------
    data: dict
        the payload of the request
        'serial_data': str or dict, the data to embed in the serial number
        'start_index': int, the starting int from which to create the incremental numbers
        'end_index': int, the end int to which end the incremental numbers
        'location_id': str, the location to attach the serial to

    Returns
    -------
    dict
        the response object with the new serial value
        'status'
        'message'
        'serials', array with the freshly created serial numbers

    Raises
    ------
    EonError
        400, something is wrong witht the input data
        409, the given serial already exist in the same location
    """
    serials = []
    for i in range(data['start_index'],1,data['end_index']):
        payload = data['serial_data']+';'+i
        try:
            single_serial_data = {'serial_data': payload, 'location_id':data['location_id']}
            new_serial = create_new_serial(single_serial_data)
            serials.append(new_serial['serial'])
        except EonError as e:
            print(e)
            #TODO: catch and report successes and errors
    return generate_batch_creation_ok_message(serials)

def check_a_serial(public_id):
    """
    Check the validations of the location of this serial number

    -Retrieve the signing company and its public key from local db,
    -check the signature
    -retrieve the public key from the company node and compare it

    Parameters
    ----------
    public_id: str
        the id on the local db of the serial to check

    Returns
    -------
    dict
        

    Raises
    ------
    EonError
        500: when the signature breaks it raises Exception

    """


    return

def get_a_serial(serial):
    return Serial.query.filter_by(serial=serial).first()

def get_serials_by_location_id(location_id):
    return Serial.query.filter_by(location_id=location_id).all()

def save_changes(data):
    db.session.add(data)
    db.session.commit()

def generate_creation_ok_message(serial):
    try:
        response_object = {
            'status': 'success',
            'message': 'New serial created.',
            'serial': serial.serial
        }
        return response_object, 201
    except Exception as e:
        print(e)
        response_object = {
            'status': 'fail',
            'message': 'Some error occurred. Please try again.'
        }
        return response_object, 401

def generate_batch_creation_ok_message(serials):
    try:
        response_object = {
            'status': 'success',
            'message': 'New serial created.',
            'serials': serials
        }
        return response_object, 201
    except Exception as e:
        print(e)
        response_object = {
            'status': 'fail',
            'message': 'Some error occurred. Please try again.'
        }
        return response_object, 401
