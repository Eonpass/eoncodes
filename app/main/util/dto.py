from flask_restplus import Namespace, fields


class UserDto:
    api = Namespace('user', description='user related operations')
    user = api.model('user', {
        'email': fields.String(required=True,
                               description='user email address'),
        'username': fields.String(required=True, description='user username'),
        'public_id': fields.String(description='user Identifier')
    })
    new_user = api.model('new_user', {
        'email': fields.String(required=True,
                               description='user email address'),
        'username': fields.String(required=True, description='user username'),
        'password': fields.String(required=True, description='user password')
    })


class AuthDto:
    api = Namespace('auth', description='authentication related operations')
    user_auth = api.model('auth_details', {
        'email': fields.String(required=True, description='The email address'),
        'password': fields.String(required=True,
                                  description='The user password'),
    })


class CompanyDto:
    api = Namespace('company', description='company related operations')
    company = api.model('company', {
        'public_id': fields.String(description='company local Identifier'),
        'name': fields.String(required=True,
                              description='company name'),
        'vat_number': fields.String(required=True,
                                description='company identificaiton number'),
        'base_url': fields.String(required=True,
                                description='base url of the node controlled by the company'),
        'public_key': fields.String(required=False,
                                description='base url of the node controlled by the company')
    })
    new_company = api.model('new_company', {
        'name': fields.String(required=True,
                              description='company name'),
        'vat_number': fields.String(required=True,
                                description='company identificaiton number'),
        'base_url': fields.String(required=True,
                                description='base url of the node controlled by the company'),
        'public_key': fields.String(required=False,
                                description='base url of the node controlled by the company')
    })
    node_owner_company = api.model('node_owner_company', {
        'public_id': fields.String(description='company local Identifier'),
        'name': fields.String(required=True,
                              description='company name'),
        'vat_number': fields.String(required=True,
                                description='company identificaiton number'),
        'base_url': fields.String(required=True,
                                description='base url of the node controlled by the company'),
        'public_key': fields.String(required=True,
                                description='base url of the node controlled by the company')
    })

class LocationDto:
    api = Namespace('location', description='location related operations')
    location = api.model('location', {
        'public_id': fields.String(description='location local identifier'),
        'name': fields.String(required=True,
                              description='location name'),
        'location_data': fields.String(required=True,
                                description='data describing the location'),
        'location_key': fields.String(required=False,
                                description='the hash of location_data signed by the connected company'),
        'company_id': fields.String(required=True,
                                description='the public id of the connected company')
    })
    new_location = api.model('new_location', {
        'name': fields.String(required=True,
                              description='company name'),
        'location_data': fields.String(required=True,
                                description='data describing the location'),
        'company_id': fields.String(required=True,
                                description='the public id of the connected company')
    })

class ValidationDto:
    api = Namespace('validation', description='validation related operations')
    validation = api.model('validation', {
        'public_id': fields.String(description='validation local identifier'),
        'created_on': fields.Date(description='Date when the validation was created in pending status'),
        'status': fields.String(required=True,
                                description='status of the validation: pending/signed'),
        'signed_location_key': fields.String(required=False,
                                description='signature of approval by the local company'),
        'signer_company_id': fields.String(required=True,
                                description='the local public id of the company who signed the location key'),
        'location_id': fields.String(required=True,
                                description='the public id of the location')
    })
    new_validation_request = api.model('new_validation_request', {
        'location_name': fields.String(required=True,
                              description='name for the location to be created (if not already present) and validated'),
        'location_data': fields.String(required=True,
                                description='data describing the location'),
        'location_key': fields.String(required=False,
                                description='the hash of location_data signed by the requesting company'),
        'company_public_key': fields.String(required=True,
                                description='the public key of the company requesting the validation')
    }) 
    validation_to_sign = api.model('validation_to_sign', {
        'public_id': fields.String(description='validation local identifier'),
        'location_id': fields.String(required=True,
                                description='the local public id of the location')
    })
    new_validation = api.model('new_validation', {
        'signer_validation_id': fields.String(required=True,
                              description='public_id on the validating company node of the validation'),
        'signed_location_key': fields.String(required=True,
                                description='The signed version of the name+location key by the validating company'),
        'location_key': fields.String(required=True,
                                description='the hash of location_data signed by the requesting company'),
        'signer_public_key': fields.String(required=True,
                                description='the public key of the company signing the validation')
    }) 

class SerialDto:
    api = Namespace('serial', description='serial related operations')
    serial = api.model('serial', {
        'serial': fields.String(required=True, description='serial number acting also as public_id'),
        'serial_data': fields.String(required=True,
                                description='data embedded in the serial'),
        'location_id': fields.String(required=True,
                                description='the location id of the respective location')
    })
    new_serial = api.model('new_serial', {
        'serial_data': fields.String(required=True,
                                description='data embedded in the serial'),
        'location_id': fields.String(required=True,
                                description='the location id of the respective location')
    })
    new_serials_batch = api.model('new_serials_batch', {
        'serial_data': fields.String(required=True,
                                description='constant data embedded in each serial'),
        'start_index': fields.Integer(require=True, description='starting index for the number appended to serial_data'),
        'end_index': fields.Integer(require=True, description='end index for the number appended to serial_data'),
        'location_id': fields.String(required=True,
                                description='the location id of the respective location')
    })



class MethodResultDto:
    api = Namespace('method_result', description='special methods rpc-like')
    method_result = api.model('method_result', {
        'method': fields.String(required=True, description='The called method'),
        'result': fields.String(required=True, description='The result'),
    })
