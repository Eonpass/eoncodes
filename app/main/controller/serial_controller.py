from flask import request
from flask_restplus import Resource

from app.main.service.serial_service import (get_a_serial, create_new_serial, create_serials_batch)
from app.main.util.dto import SerialDto
from app.main.util.decorator import admin_token_required, token_required
from app.main.util.eonerror import EonError

api = SerialDto.api
_serial = SerialDto.serial
_new_serial = SerialDto.new_serial
_new_serials_batch = SerialDto.new_serials_batch

parser = api.parser()
parser.add_argument('Authorization', location='headers', help="Auth token from login")


@api.route('/')
class NewSerial(Resource):
    @api.doc('Create a new serials')
    @api.expect(parser, _new_serial, validate=True)
    @api.response(201, 'Serial successfully created.')
    @api.response(400, 'Serial input data is invalid.')
    @api.response(500, 'Internal Server Error.')
    def post(self):
        """Create a new serials"""
        data = request.json
        try:
            return create_new_serial(data=data)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)


@api.route('/rpc/crate_serials_batch')
class SerialBatch(Resource):
    @api.doc('Create a batch of serials')
    @api.expect(parser, _new_serials_batch, validate=True)
    @api.response(201, 'Serials successfully created.')
    @api.response(400, 'Serials input data is invalid.')
    @api.response(500, 'Internal Server Error.')
    def post(self):
        """Create a batch of serials"""
        data = request.json
        try:
            return create_serials_batch(data=data)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)


@api.route('/<serial>')
@api.param('serial', 'The serial identifier')
class Serial(Resource):
    @api.doc('get a serial')
    @api.marshal_with(_serial)
    @api.response(404, 'Validation not found.')
    @api.response(500, 'Internal Server Error.')
    def get(self, serial):
        """Get details for a single serial""" 
        try:
            return get_a_serial(serial)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)